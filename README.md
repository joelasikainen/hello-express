# Hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the dependencies

```sh
npm i
```

Start the REST API service:

`node app.js`

Or in the latest NodeJS version (>=20):

`node --watch app.js`


<img src="assets/images/tux-158547_640.png" width="300" height="300">